<?php
// $Id $

/**
 * @file
 * Display statistics of different site entities
 */

/**
 * Return an array of user's account summary.
 */
function site_tracker_user_summary() {
  $output = '';

  $user_count_all  = db_query('SELECT COUNT(uid) FROM {users}')->fetchField();
  $user_count_active  = db_query('SELECT COUNT(uid) FROM {users} WHERE status = 1')->fetchField();

  $output .= '<div class="user-tracker-field">
                <div class="tracker-field-label">Total user count: ' . number_format($user_count_all) . '</div>
                <div class="tracker-field-data">' .
                'Active = ' . number_format($user_count_active) . ' (' . round($user_count_active / $user_count_all * 100) . '%)&nbsp;&nbsp; ' .
                'Blocked = ' . number_format($user_count_all - $user_count_active) . ' (' . round(($user_count_all - $user_count_active) / $user_count_all * 100) . '%)' .
                '</div>
              </div>';

  $user_count_login  = db_query('SELECT COUNT(uid) FROM {users} WHERE login > 0')->fetchField();

  $output .= '<div class="user-tracker-field">
                <div class="tracker-field-label"></div>
                <div class="tracker-field-data">' .
                'Login = ' . number_format($user_count_login) . ' (' . round($user_count_login / $user_count_all * 100) . '%)&nbsp;&nbsp; ' .
                'Never login = ' . number_format($user_count_all - $user_count_login) . ' (' . round(($user_count_all - $user_count_login) / $user_count_all * 100) . '%)' .
                '</div>
              </div>';

  $sql_count_login_week = 'SELECT COUNT(uid) FROM {users} WHERE login >= :counter_login_week';
  $user_count_login_week = db_query($sql_count_login_week, array(':counter_login_week' => time() - 86400 * 7))->fetchField();

  $sql_count_login_month = 'SELECT COUNT(uid) FROM {users} WHERE login >= :counter_login_month';
  $user_count_login_month = db_query($sql_count_login_month, array(':counter_login_month' => time() - 86400 * 30.4))->fetchField();

  $sql_count_login_year = 'SELECT COUNT(uid) FROM {users} WHERE login >= :counter_login_year';
  $user_count_login_year = db_query($sql_count_login_year, array(':counter_login_year' => time() - 86400 * 365))->fetchField();

  $output .= '<div class="user-tracker-field">
                <div class="tracker-field-label">Logged in accounts:</div>
                <div class="tracker-field-data">' .
                'Last week = ' . number_format($user_count_login_week) . '&nbsp;&nbsp; ' .
                'Last month = ' . number_format($user_count_login_month) . '&nbsp;&nbsp; ' .
                'Last year = ' . number_format($user_count_login_year) . '&nbsp;&nbsp; ' .
                'Over a year = ' . number_format($user_count_login - $user_count_login_year) . '&nbsp;&nbsp; ' .
                '</div>
             </div>';


  return $output;
}

/**
 * Queries the database for info, generates the render array that will be used to render the page.
 *
 * Returns an array of top domains by user email.
 */
function site_tracker_user() {
  drupal_set_title(t('Site Tracker'));

  $rows = array();
  
  $query = "SELECT COUNT(uid) count, SUBSTR(mail, LOCATE('@', mail) + 1) AS domain FROM {users} WHERE LENGTH(mail) > 0 AND SUBSTR(mail, 1, 1) <> '#' AND status = 1 GROUP BY SUBSTR(mail, LOCATE('@', mail) + 1) ORDER BY COUNT(uid) DESC";
  $result = db_query_range($query, 0, 50);

  foreach ($result as $data) {
      $row = array(
        'domain' => array('class' => array('cell-field-name'), 'data' => check_plain($data->domain)),
        'count' => array('class' => array('cell-field-count'), 'data' => $data->count),
      );
      $rows[] = $row;
  }

  $page['domain_tracker'] = array(
    '#rows' => $rows,
    '#header' => array(t('Domain'), t('Count')),
    '#caption' => 'Top domains by user email:',    //Optional Caption for the table
    '#theme' => 'table',
    '#empty' => t('No content available.'),
    '#attached' => array(
      'css' => array(drupal_get_path('module', 'site_tracker') . '/site_tracker.css' => array()),
    ),
  );
  return $page;
}

/**
 * Returns html of node and node type count.
 */
function site_tracker_node_summary() {
  $output = '';

  $nodetype_count_all  = db_query('SELECT COUNT(type) FROM {node_type}')->fetchField();
  $node_count_all  = db_query('SELECT COUNT(nid) FROM {node}')->fetchField();

  $output .= '<div class="node-summary-wrapper">
                <div class="node-summary-type">Total Node Type count: <span class="bold">' . $nodetype_count_all . '</span></div>
                <div class="node-summary-node">Total Node Count: <span class="bold">' . $node_count_all . '</span></div>
              </div>';

  return $output;
}

/**
 * Queries the database for info, generates the render array that will be used to render the page.
 *
 * Returns an array of node type and node count.
 */
function site_tracker_node() {
  drupal_set_title(t('Site Tracker')); //Can overwrite title here

  //Node type array
  $nodetypes = node_type_get_names();

  $rows = array();
  foreach ($nodetypes as $nodetype => $nodetype_name) {
    $published_node_count = 0;
    //Get published node count for each node type
    $sql_published_node = 'SELECT COUNT(n.nid) AS node_count FROM {node} n WHERE n.status = 1 AND n.type = :node_type';
    $published_node_count = db_query($sql_published_node, array(':node_type' => $nodetype))->fetchField();

    $unpublished_node_count = 0;
    //Get un-published node count for each node type
    $sql_unpublished_node = 'SELECT COUNT(n.nid) AS node_count FROM {node} n WHERE n.status = 0 AND n.type = :node_type';
    $unpublished_node_count = db_query($sql_unpublished_node, array(':node_type' => $nodetype))->fetchField();

    $row = array(
        'name' => array('class' => array('cell-field-name'), 'data' => check_plain($nodetype_name)),
        'p-node' => array('class' => array('cell-field-count'), 'data' => $published_node_count),
        'up-node' => array('class' => array('cell-field-count'), 'data' => $unpublished_node_count),
    );
    $rows[] = $row;
  }

  $page['content_tracker'] = array(
    '#rows' => $rows,
    '#header' => array(t('Type'), t('Published Node Count'), t('Un-Published Node Count')),
    '#caption' => 'Published and Un-Published Node count for each Node Type:',    //Optional Caption for the table
    '#theme' => 'table',
    '#empty' => t('No content available.'),
    '#attached' => array(
      'css' => array(drupal_get_path('module', 'site_tracker') . '/site_tracker.css' => array()),
    ),
  );
  return $page;
}

/**
 * Returns html of taxonomy count.
 */
function site_tracker_taxonomy_summary() {
  $output = '';

  $vocabulary_count_all  = db_query('SELECT COUNT(vid) FROM {taxonomy_vocabulary}')->fetchField();
  $term_count_all  = db_query('SELECT COUNT(tid) FROM {taxonomy_term_data}')->fetchField();

  $output .= '<div class="taxonomy-summary-wrapper">
                <div class="vocabulary-summary">Total vocabulary count: <span class="bold">' . $vocabulary_count_all . '</span></div>
                <div class="term-summary">Total term Count: <span class="bold">' . $term_count_all . '</span></div>
              </div>';

  return $output;
}

/**
 * Queries the database for info, generates the render array that will be used to render the page.
 *
 * Returns an array of vocabulary and term count.
 */
function site_tracker_taxonomy() {
  drupal_set_title(t('Site Tracker')); //Can overwrite title here

  //Vocabulary array
  $vocabularies = taxonomy_get_vocabularies();

  $rows = array();
  foreach ($vocabularies as $vocabulary) {
      $term_count = 0;
      //Get term count for each vocabulary
      $sql_term = 'SELECT COUNT(term.tid) AS term_count FROM {taxonomy_term_data} term WHERE term.vid = :vid';
      $term_count = db_query($sql_term, array(':vid' => $vocabulary->vid))->fetchField();

      $node_count = 0;
      //Get node count for each vocabulary
      $sql_node = 'SELECT COUNT(tindex.nid) AS node_count FROM {taxonomy_index} tindex INNER JOIN {taxonomy_term_data} tdata ON tdata.tid = tindex.tid WHERE tdata.vid = :vid';
      $node_count = db_query($sql_node, array(':vid' => $vocabulary->vid))->fetchField();

      $row = array(
        'name' => array('class' => array('cell-field-name'), 'data' => check_plain($vocabulary->name)),
        'term' => array('class' => array('cell-field-count'), 'data' => $term_count),
        'node' => array('class' => array('cell-field-count'), 'data' => $node_count),
      );
      $rows[] = $row;
  }

  $page['taxonomy_tracker'] = array(
    '#rows' => $rows,
    '#header' => array(t('Vocabulary'), t('Term Count'), t('Node Count')),
    '#caption' => 'Term count and Node count for each Vocabulary:',   //Optional Caption for the table
    '#theme' => 'table',
    '#empty' => t('No content available.'),
    '#attached' => array(
      'css' => array(drupal_get_path('module', 'site_tracker') . '/site_tracker.css' => array()),
    ),
  );
  return $page;
}